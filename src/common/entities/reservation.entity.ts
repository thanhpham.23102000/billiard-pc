import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { TableCategoryEntity } from './table-category.entity';

export enum ReservationStatus {
  COMPLETED = 'completed',
  CANCELED = 'canceled',
  PENDING = 'pending',
}

@Entity({ name: 'reservations' })
export class ReservationEntity extends BaseEntity {
  constructor(partial: Partial<ReservationEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', nullable: true })
  email: string;

  @Column({ type: 'enum', enum: ReservationStatus, nullable: false })
  status!: ReservationStatus;

  @Column({ type: 'varchar', nullable: true })
  phoneNumber: string;

  // YYYY-MM-DD hh:mm:ss
  @Column({ type: 'datetime' })
  bookingTime: Date;

  @ManyToOne(
    () => TableCategoryEntity,
    (tableCategory) => tableCategory.reservations,
  )
  tableCategory!: TableCategoryEntity;
}
