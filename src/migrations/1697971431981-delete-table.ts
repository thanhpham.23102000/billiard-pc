import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeleteTable1697971431981 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // Delete the tables
    await queryRunner.query(
      'DROP TABLE IF EXISTS addtional_item_order_details',
    );
    await queryRunner.query('DROP TABLE IF EXISTS addtional_item_order');
    await queryRunner.query('DROP TABLE IF EXISTS table_order_details');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async down(queryRunner: QueryRunner): Promise<void> {
    // This is the rollback logic, but for deleting tables, there's no need for a rollback.
  }
}
