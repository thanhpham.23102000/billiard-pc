import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ReservationEntity } from 'src/common/entities';
import { CreateReservationDto } from './reservation.dto';
import { ReservationService } from './reservation.service';

@Controller('reservation')
@ApiTags('reservation')
export class ReservationController {
  constructor(private readonly _reservationService: ReservationService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: ReservationEntity })
  async createReservation(
    @Body() request: CreateReservationDto,
  ): Promise<ReservationEntity> {
    return this._reservationService.createReservation(request);
  }
}
