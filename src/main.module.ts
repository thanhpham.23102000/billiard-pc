import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import configurations from './config';
import { TableModule } from './modules/table/table.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ResponseInterceptor } from './common/interceptors/response.interceptor';
import { TimeoutInterceptor } from './common/interceptors/timeout.interceptor';
import { DatabaseModule } from './modules/db/db.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TableCategoryModule } from './modules/table-category/table-category.module';
import { ReservationModule } from './modules/reservation/reservation.module';
import { OrderModule } from './modules/order/order.module';
import { AdditionalItemModule } from './modules/additional-item/additional-item.module';
import { TableOrderModule } from './modules/table-order/table-order.module';
import { AdditionalItemOrderModule } from './modules/additional-item-order/additional-item-order.module';
import { WinstonModule } from 'nest-winston';
import { configLog } from './config/const';

const modules = [
  TableModule,
  DatabaseModule,
  TableCategoryModule,
  ReservationModule,
  OrderModule,
  AdditionalItemModule,
  TableOrderModule,
  AdditionalItemOrderModule,
];

@Module({
  imports: [
    ConfigModule.forRoot({
      load: configurations,
      isGlobal: true,
      envFilePath: process.env.NODE_ENV
        ? `.env.${process.env.NODE_ENV}`
        : '.env',
    }),
    WinstonModule.forRootAsync({
      useFactory: (config: ConfigService): any => {
        return config.get(configLog);
      },
      inject: [ConfigService],
    }),
    ...modules,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
    // can customize the response as desired
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
  ],
})
export class MainModule {}
