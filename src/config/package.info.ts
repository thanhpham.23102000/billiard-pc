import { name, version, description } from '../../package.json';

export const PackageInfo = {
  name: name,
  description: description,
  version: version,
};
