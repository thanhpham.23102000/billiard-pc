import {
  ORDER_REPOSITORY,
  TABLE_ORDER_REPOSITORY,
  TABLE_REPOSITORY,
} from 'src/common/constants';
import {
  OrderEntity,
  TableEntity,
  TableOrderEntity,
} from 'src/common/entities';
import { configMySQLDataSource } from 'src/config/const';
import { DataSource } from 'typeorm';

export const tableOrderProviders = [
  {
    provide: TABLE_ORDER_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TableOrderEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: TABLE_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TableEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: ORDER_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(OrderEntity),
    inject: [configMySQLDataSource],
  },
];
