import { registerAs } from '@nestjs/config';
import { SnakeNamingStrategy } from '../common/strategies/snake-naming.strategy';
import * as dotenv from 'dotenv';

if (process.env.NODE_ENV === 'production') {
  dotenv.config({ path: '.env.production' });
} else {
  dotenv.config({ path: '.env' });
}

export const configMysql = {
  type: process.env.ORM_CONNECTION,
  host: process.env.ORM_HOST,
  port: parseInt(process.env.ORM_PORT!, 10) || 3306,
  username: process.env.ORM_USERNAME,
  password: process.env.ORM_PASSWORD,
  database: process.env.ORM_DB,
  timezone: 'Z',
  logging: process.env.ORM_LOGGING === 'true',
  autoLoadEntities: true,
  keepConnectionAlive: true,
  entities: [`${__dirname}/../common/entities/*.entity{.ts,.js}`],
  migrations: [`${__dirname}/../migrations/*{.ts,.js}`],
  // synchronize: process.env.NODE_ENV === 'local', // shouldn't be used in production - otherwise you can lose production data.
  synchronize: false, // shouldn't be used in production - otherwise you can lose production data.
  namingStrategy: new SnakeNamingStrategy(),
  extra: {
    connectionLimit: 10,
  },
};

export default registerAs('mysql', () => configMysql);
