import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { reservationProviders } from './reservation.provider';
import { ReservationController } from './reservation.controller';
import { ReservationService } from './reservation.service';

@Module({
  imports: [DatabaseModule],
  controllers: [ReservationController],
  providers: [...reservationProviders, ReservationService],
})
export class ReservationModule {}
