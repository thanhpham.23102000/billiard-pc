import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { TableCategoryService } from './table-category.service';
import {
  CreateTableCategoryDto,
  UpdateTableCategoryDto,
} from './table-category.dto';
import { TableCategoryEntity } from 'src/common/entities';

@Controller('table-category')
@ApiTags('table-category')
export class TableCategoryController {
  constructor(private readonly _tableCategoryService: TableCategoryService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: TableCategoryEntity })
  async createTableCategory(
    @Body() request: CreateTableCategoryDto,
  ): Promise<TableCategoryEntity> {
    return this._tableCategoryService.createTableCategory(request);
  }

  @Put()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: TableCategoryEntity })
  async updateTableCategory(
    @Body() request: UpdateTableCategoryDto,
  ): Promise<TableCategoryEntity> {
    return this._tableCategoryService.updateTableCategory(request);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    type: TableCategoryEntity,
    description: 'Delete table category',
  })
  async deleteTableCategory(@Param('id') id: string): Promise<void> {
    return this._tableCategoryService.deleteTableCategory(id);
  }

  @Put('restore/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    type: TableCategoryEntity,
    description: 'Restore table category',
  })
  async restoreTableCategory(
    @Param('id') id: string,
  ): Promise<TableCategoryEntity> {
    return this._tableCategoryService.restoreTableCategory(id);
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: TableCategoryEntity,
    description: 'Get one table category',
  })
  async getOneTableCategory(
    @Param('id') id: string,
  ): Promise<TableCategoryEntity> {
    return this._tableCategoryService.getOneTableCategory(id);
  }
}
