import { MigrationInterface, QueryRunner } from 'typeorm';

export class DbInitiation1696179303594 implements MigrationInterface {
  name = 'DbInitiation1696179303594';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`reservations\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`email\` varchar(255) NULL, \`status\` enum ('completed', 'canceled', 'pending') NOT NULL, \`phone_number\` varchar(255) NULL, \`booking_time\` date NOT NULL, \`table_category_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`table_categories\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`name\` varchar(255) NOT NULL, \`unit_price\` decimal(18,6) NOT NULL, \`description\` varchar(255) NULL, UNIQUE INDEX \`IDX_8f365bd67957d2b6b59967c4f9\` (\`name\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`tables\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`name\` varchar(255) NOT NULL, \`status\` enum ('available', 'unavailable', 'booked') NOT NULL DEFAULT 'available', \`table_category_id\` varchar(36) NULL, UNIQUE INDEX \`IDX_672c2f353f696989bb92d5e799\` (\`name\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`orders\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`email\` varchar(255) NULL, \`status\` enum ('completed', 'canceled', 'pending') NOT NULL, \`phone_number\` varchar(255) NULL, \`customer_name\` varchar(255) NULL, \`total_price\` decimal(18,6) NOT NULL, \`payment_method\` enum ('cash', 'banking') NOT NULL, \`booking_time\` date NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`order_details\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`start_time\` date NULL, \`end_time\` date NULL, \`quantity\` int NOT NULL, \`net_price\` decimal(18,6) NOT NULL, \`additional_item_id\` varchar(36) NULL, \`table_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`additional_items\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`name\` varchar(255) NOT NULL, \`price\` decimal(18,6) NOT NULL, UNIQUE INDEX \`IDX_edb384ed5720706e8600045df4\` (\`name\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` ADD CONSTRAINT \`FK_9f3971edf6e9c0416d30a6db862\` FOREIGN KEY (\`table_category_id\`) REFERENCES \`table_categories\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`tables\` ADD CONSTRAINT \`FK_b241ce2efcb510660cf0c7811c2\` FOREIGN KEY (\`table_category_id\`) REFERENCES \`table_categories\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` ADD CONSTRAINT \`FK_5c89b5ffbfdcbd65e54b3daf354\` FOREIGN KEY (\`additional_item_id\`) REFERENCES \`additional_items\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` ADD CONSTRAINT \`FK_121d6e104bfb66257b021971eda\` FOREIGN KEY (\`table_id\`) REFERENCES \`tables\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` ADD CONSTRAINT \`FK_3ff3367344edec5de2355a562ee\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`order_details\` DROP FOREIGN KEY \`FK_3ff3367344edec5de2355a562ee\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` DROP FOREIGN KEY \`FK_121d6e104bfb66257b021971eda\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` DROP FOREIGN KEY \`FK_5c89b5ffbfdcbd65e54b3daf354\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`tables\` DROP FOREIGN KEY \`FK_b241ce2efcb510660cf0c7811c2\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` DROP FOREIGN KEY \`FK_9f3971edf6e9c0416d30a6db862\``,
    );
    await queryRunner.query(
      `DROP INDEX \`IDX_edb384ed5720706e8600045df4\` ON \`additional_items\``,
    );
    await queryRunner.query(`DROP TABLE \`additional_items\``);
    await queryRunner.query(`DROP TABLE \`order_details\``);
    await queryRunner.query(`DROP TABLE \`orders\``);
    await queryRunner.query(
      `DROP INDEX \`IDX_672c2f353f696989bb92d5e799\` ON \`tables\``,
    );
    await queryRunner.query(`DROP TABLE \`tables\``);
    await queryRunner.query(
      `DROP INDEX \`IDX_8f365bd67957d2b6b59967c4f9\` ON \`table_categories\``,
    );
    await queryRunner.query(`DROP TABLE \`table_categories\``);
    await queryRunner.query(`DROP TABLE \`reservations\``);
  }
}
