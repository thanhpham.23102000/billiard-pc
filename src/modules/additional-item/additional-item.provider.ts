import { ADDITIONAL_ITEM_REPOSITORY } from 'src/common/constants';
import { AdditionalItemEntity } from 'src/common/entities';
import { configMySQLDataSource } from 'src/config/const';
import { DataSource } from 'typeorm';

export const additionalItemProviders = [
  {
    provide: ADDITIONAL_ITEM_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(AdditionalItemEntity),
    inject: [configMySQLDataSource],
  },
];
