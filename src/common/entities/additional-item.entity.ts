import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { AdditionalItemOrderEntity } from './additional-item-order.entity';

@Entity({ name: 'additional_items' })
export class AdditionalItemEntity extends BaseEntity {
  constructor(partial: Partial<AdditionalItemEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', unique: true, nullable: false })
  name!: string;

  @Column({ type: 'decimal', precision: 18, scale: 6, nullable: false })
  price!: number;

  @OneToMany(
    () => AdditionalItemOrderEntity,
    (additionalItemOrder) => additionalItemOrder.additionalItem,
  )
  additionalItemOrders: AdditionalItemOrderEntity[];
}
