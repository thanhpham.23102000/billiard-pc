import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { additionalItemOrderProviders } from './additional-item-order.provider';
import { AdditionalItemOrderController } from './additional-item-order.controller';
import { AdditionalItemOrderService } from './additional-item-order.service';

@Module({
  imports: [DatabaseModule],
  controllers: [AdditionalItemOrderController],
  providers: [...additionalItemOrderProviders, AdditionalItemOrderService],
})
export class AdditionalItemOrderModule {}
