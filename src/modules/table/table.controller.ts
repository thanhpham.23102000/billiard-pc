import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateTableDto } from './table.dto';
import { TableService } from './table.service';
import { TableEntity } from 'src/common/entities';

@Controller('table')
@ApiTags('table')
export class TableController {
  constructor(private readonly _tableService: TableService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: TableEntity })
  async createTable(@Body() request: CreateTableDto): Promise<TableEntity> {
    return this._tableService.createTable(request);
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: TableEntity,
    description: 'Get one table',
  })
  async getOneTableCategory(@Param('id') id: string): Promise<TableEntity> {
    return this._tableService.getOneTable(id);
  }
}
