import { Inject, Injectable } from '@nestjs/common';
import { ORDER_REPOSITORY } from 'src/common/constants';
import { OrderEntity } from 'src/common/entities';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './order.dto';
import { InternalServerException } from 'src/common/exceptions';

@Injectable()
export class OrderService {
  constructor(
    @Inject(ORDER_REPOSITORY)
    private readonly _orderRepository: Repository<OrderEntity>,
  ) {}

  public async createOrder(orderDto: CreateOrderDto): Promise<OrderEntity> {
    try {
      const order = new OrderEntity(orderDto);
      const res = await this._orderRepository.save(order);
      return res;
    } catch (error: any) {
      throw new InternalServerException(`Error creating order: ${error}`);
    }
  }
}
