import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { ReservationStatus } from 'src/common/entities';

export class CreateReservationDto {
  @IsOptional()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsEnum(ReservationStatus)
  status!: ReservationStatus;

  @IsNotEmpty()
  @IsString()
  phoneNumber!: string;

  @IsNotEmpty()
  @IsDateString()
  bookingTime!: Date;

  @IsNotEmpty()
  @IsString()
  tableCategoryId: string;
}
