import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { OrderEntity } from 'src/common/entities';
import { CreateOrderDto } from './order.dto';
import { OrderService } from './order.service';

@Controller('order')
@ApiTags('order')
export class OrderController {
  constructor(private readonly _orderService: OrderService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: OrderEntity })
  async createOrder(@Body() request: CreateOrderDto): Promise<OrderEntity> {
    return this._orderService.createOrder(request);
  }
}
