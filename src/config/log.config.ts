import { registerAs } from '@nestjs/config';
import { format, transports, transport } from 'winston';
import { PackageInfo } from './package.info';

const { combine, label, timestamp, json } = format;

export default registerAs('log', () => {
  const configTransports: transport[] = [
    new transports.Console({
      format: combine(
        label({ label: `${PackageInfo.name}-${PackageInfo.version}` }),
        timestamp(),
        json(),
      ),
      level: 'info',
      silent: process.env.NODE_ENV === 'test',
    }),
    new transports.Console({
      format: combine(
        label({ label: `${PackageInfo.name}-${PackageInfo.version}` }),
        timestamp(),
        json(),
      ),
      level: 'error',
    }),
  ];
  return {
    transports: configTransports,
  };
});
