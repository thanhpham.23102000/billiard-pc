import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveIsDeleteColumn1697039539185 implements MigrationInterface {
  name = 'RemoveIsDeleteColumn1697039539185';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`additional_items\` DROP COLUMN \`is_deleted\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` DROP COLUMN \`is_deleted\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_categories\` DROP COLUMN \`is_deleted\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`tables\` DROP COLUMN \`is_deleted\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` DROP COLUMN \`is_deleted\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`order_details\` DROP COLUMN \`is_deleted\``,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`order_details\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`tables\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_categories\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`additional_items\` ADD \`is_deleted\` tinyint NOT NULL DEFAULT '0'`,
    );
  }
}
