import { ORDER_REPOSITORY } from 'src/common/constants';
import { OrderEntity } from 'src/common/entities';
import { configMySQLDataSource } from 'src/config/const';
import { DataSource } from 'typeorm';

export const orderProviders = [
  {
    provide: ORDER_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(OrderEntity),
    inject: [configMySQLDataSource],
  },
];
