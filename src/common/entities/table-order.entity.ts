import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { TableEntity } from './table.entity';
import { OrderEntity } from './order.entity';

@Entity({ name: 'table_orders' })
export class TableOrderEntity extends BaseEntity {
  constructor(partial: Partial<TableOrderEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  startTime: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  endTime: Date;

  @Column({ type: 'decimal', precision: 18, scale: 6, nullable: false })
  netPrice: number;

  @ManyToOne(() => TableEntity, (table) => table.tableOrders)
  table!: TableEntity;

  @ManyToOne(() => OrderEntity, (order) => order.tableOrders)
  order!: OrderEntity;
}
