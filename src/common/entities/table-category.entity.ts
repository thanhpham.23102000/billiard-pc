import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { Expose } from 'class-transformer';
import { TableEntity } from './table.entity';
import { ReservationEntity } from './reservation.entity';

@Entity({ name: 'table_categories' })
export class TableCategoryEntity extends BaseEntity {
  constructor(partial: Partial<TableCategoryEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', unique: true, nullable: false })
  name!: string;

  @Column({ type: 'decimal', precision: 18, scale: 6, nullable: false })
  unitPrice!: number;

  @Column({ type: 'varchar', nullable: true })
  description: string;

  // no impact on creating the migration
  // for testing purpose ClassSerializerInterceptor
  @Expose()
  get nameAndPrice(): string {
    return `${this.name} ${this.unitPrice}`;
  }

  @OneToMany(() => TableEntity, (table) => table.tableCategory)
  tables: TableEntity[];

  @OneToMany(
    () => ReservationEntity,
    (reservation) => reservation.tableCategory,
  )
  reservations: ReservationEntity[];
}
