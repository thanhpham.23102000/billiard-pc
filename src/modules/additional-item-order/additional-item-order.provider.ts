import {
  ADDITIONAL_ITEM_ORDER_REPOSITORY,
  ADDITIONAL_ITEM_REPOSITORY,
  ORDER_REPOSITORY,
} from 'src/common/constants';
import {
  AdditionalItemEntity,
  AdditionalItemOrderEntity,
  OrderEntity,
} from 'src/common/entities';
import { configMySQLDataSource } from 'src/config/const';
import { DataSource } from 'typeorm';

export const additionalItemOrderProviders = [
  {
    provide: ADDITIONAL_ITEM_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(AdditionalItemEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: ADDITIONAL_ITEM_ORDER_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(AdditionalItemOrderEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: ORDER_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(OrderEntity),
    inject: [configMySQLDataSource],
  },
];
