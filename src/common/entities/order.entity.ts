import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { TableOrderEntity } from './table-order.entity';
import { AdditionalItemOrderEntity } from './additional-item-order.entity';

export enum OrderStatus {
  COMPLETED = 'completed',
  CANCELED = 'canceled',
  PENDING = 'pending',
}

export enum PaymentMethod {
  CASH = 'cash',
  BANKING = 'banking',
}

@Entity({ name: 'orders' })
export class OrderEntity extends BaseEntity {
  constructor(partial: Partial<OrderEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', nullable: true })
  email: string;

  @Column({
    type: 'enum',
    enum: OrderStatus,
    nullable: false,
    default: OrderStatus.PENDING,
  })
  status!: OrderStatus;

  @Column({ type: 'varchar', nullable: true })
  phoneNumber: string;

  @Column({ type: 'varchar', nullable: true })
  customerName: string;

  @Column({
    type: 'decimal',
    precision: 18,
    scale: 6,
    nullable: true,
    default: 0,
  })
  totalPrice!: number;

  @Column({ type: 'enum', enum: PaymentMethod, nullable: true })
  paymentMethod!: PaymentMethod;

  @OneToMany(() => TableOrderEntity, (tableOrder) => tableOrder.order)
  tableOrders: TableOrderEntity[];

  @OneToMany(
    () => AdditionalItemOrderEntity,
    (additionalItemOrder) => additionalItemOrder.order,
  )
  additionalItemOrder: AdditionalItemOrderEntity[];
}
