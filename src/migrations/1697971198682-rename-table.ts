import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameTable1697971198682 implements MigrationInterface {
  name = 'RenameTable1697971198682';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`addtional_item_orders\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`quantity\` int NOT NULL, \`additional_item_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_orders\` ADD CONSTRAINT \`FK_1efb07525ff4c7232c693237b69\` FOREIGN KEY (\`additional_item_id\`) REFERENCES \`additional_items\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_orders\` ADD CONSTRAINT \`FK_d72397ff319c97b54b34edc3e1b\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_orders\` DROP FOREIGN KEY \`FK_d72397ff319c97b54b34edc3e1b\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_orders\` DROP FOREIGN KEY \`FK_1efb07525ff4c7232c693237b69\``,
    );
    await queryRunner.query(`DROP TABLE \`addtional_item_orders\``);
  }
}
