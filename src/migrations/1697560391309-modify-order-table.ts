import { MigrationInterface, QueryRunner } from 'typeorm';

export class ModifyOrderTable1697560391309 implements MigrationInterface {
  name = 'ModifyOrderTable1697560391309';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`orders\` DROP COLUMN \`booking_time\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`status\` \`status\` enum ('completed', 'canceled', 'pending') NOT NULL DEFAULT 'pending'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`total_price\` \`total_price\` decimal(18,6) NULL DEFAULT '0.000000'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`payment_method\` \`payment_method\` enum ('cash', 'banking') NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`payment_method\` \`payment_method\` enum ('cash', 'banking') NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`total_price\` \`total_price\` decimal(18,6) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` CHANGE \`status\` \`status\` enum ('completed', 'canceled', 'pending') NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` ADD \`booking_time\` datetime NOT NULL`,
    );
  }
}
