import { IsNotEmpty, IsString, IsEnum, IsOptional } from 'class-validator';
import { AbstractUpdateDto } from 'src/common/dtos';
import { TableStatus } from 'src/common/entities';

export class CreateTableDto {
  @IsNotEmpty()
  @IsString()
  name!: string;

  @IsNotEmpty()
  @IsEnum(TableStatus)
  status!: TableStatus;

  @IsNotEmpty()
  @IsString()
  tableCategoryId: string;
}

export class UpdateTableDto extends AbstractUpdateDto {
  @IsOptional()
  @IsString()
  name?: string;

  @IsOptional()
  @IsEnum(TableStatus)
  status?: TableStatus;

  @IsOptional()
  @IsString()
  tableCategoryId: string;
}
