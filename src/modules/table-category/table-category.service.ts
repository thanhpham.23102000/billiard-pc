import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { TableCategoryEntity, TableEntity } from 'src/common/entities';
import {
  TABLE_CATEGORY_REPOSITORY,
  TABLE_REPOSITORY,
} from 'src/common/constants';
import {
  CreateTableCategoryDto,
  UpdateTableCategoryDto,
} from './table-category.dto';
import { BadRequestException } from 'src/common/exceptions';

@Injectable()
export class TableCategoryService {
  constructor(
    @Inject(TABLE_REPOSITORY)
    private readonly _tableRepository: Repository<TableEntity>,
    @Inject(TABLE_CATEGORY_REPOSITORY)
    private readonly _tableCategoryRepository: Repository<TableCategoryEntity>,
  ) {}

  public async createTableCategory(
    createTableCategoryDto: CreateTableCategoryDto,
  ): Promise<TableCategoryEntity> {
    try {
      const tableCategory = new TableCategoryEntity(createTableCategoryDto);
      const res = await this._tableCategoryRepository.save(tableCategory);
      return res;
    } catch (e) {
      throw new HttpException(
        `Error creating table category: ${e}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async updateTableCategory(
    updateTableCategoryDto: UpdateTableCategoryDto,
  ): Promise<TableCategoryEntity> {
    try {
      await this._tableCategoryRepository.update(
        {
          id: updateTableCategoryDto.id,
        },
        {
          ...updateTableCategoryDto,
        },
      );
      const updatedTableCategory =
        await this._tableCategoryRepository.findOneBy({
          id: updateTableCategoryDto.id,
        });
      return updatedTableCategory;
    } catch (e) {
      throw new HttpException(
        `Error creating table category: ${e}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async deleteTableCategory(id: string): Promise<void> {
    const tableCategory = await this._tableCategoryRepository.findOneBy({
      id,
    });

    if (!tableCategory) {
      throw new BadRequestException('Table category not found');
    }

    await this._tableCategoryRepository.softDelete(id);
  }

  public async restoreTableCategory(id: string): Promise<TableCategoryEntity> {
    await this._tableCategoryRepository.restore(id);
    const tableCategory = await this._tableCategoryRepository.findOneBy({
      id,
    });

    if (!tableCategory) {
      throw new BadRequestException('Table category not found');
    }
    return tableCategory;
  }

  public async getOneTableCategory(id: string): Promise<TableCategoryEntity> {
    const tableCategory = await this._tableCategoryRepository.findOne({
      where: {
        id,
      },
      relations: ['tables'],
    });

    if (!tableCategory) {
      throw new BadRequestException('Table category not found');
    }

    return tableCategory;
  }
}
