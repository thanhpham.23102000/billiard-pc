import { IsNotEmpty, IsPositive, IsString } from 'class-validator';

export class CreateAdditionalItemOrderDto {
  @IsNotEmpty()
  @IsString()
  additionalItemId: string;

  @IsNotEmpty()
  @IsString()
  orderId: string;

  @IsNotEmpty()
  @IsPositive()
  quantity: number;
}
