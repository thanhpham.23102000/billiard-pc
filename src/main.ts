import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import { HttpExceptionFilter } from './common/exceptions';
import * as compression from 'compression';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(MainModule);
  // speed up response processing
  app.use(compression());
  app.setGlobalPrefix('api');
  app.useGlobalFilters(new HttpExceptionFilter());

  // remove redundant property in the request body and transform dto automatically
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      // to do: validate custom pipe
    }),
  );

  if (process.env.NODE_ENV !== 'production') {
    // yourdomain/docs-api -> view swagger docs
    const config = new DocumentBuilder()
      .setTitle('PC-Note application')
      .setDescription('Note and other functionalities')
      .setVersion('v1')
      .addTag('note')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('docs-api', app, document, {
      customSiteTitle: 'PC-Note application',
      swaggerOptions: {
        docExpansion: 'list',
        filter: true,
        displayRequestDuration: true,
      },
    });
  }

  await app.listen(parseInt(process.env.PORT, 10) || 3000);
}
bootstrap();
