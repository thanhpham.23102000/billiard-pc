import { ConfigService } from '@nestjs/config';
import { configMysqlDb, configMySQLDataSource } from '../../config/const';
import { DataSource, DataSourceOptions } from 'typeorm';
import { configMysql } from '../../config/mysql.config';

export const mysqlProviders = [
  {
    provide: configMySQLDataSource,
    useFactory: async (configService: ConfigService) => {
      const dataSource = new DataSource(configService.get(configMysqlDb));
      await dataSource
        .initialize()
        .then(() => {
          console.log('Data Source has been initialized!');
        })
        .catch((err) => {
          console.error('Error during Data Source initialization', err);
        });
      return dataSource;
    },
    inject: [ConfigService],
  },
];

const datasourceConfigToMigration = new DataSource(
  configMysql as DataSourceOptions,
);

export default datasourceConfigToMigration;
