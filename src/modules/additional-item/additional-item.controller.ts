import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AdditionalItemEntity } from 'src/common/entities';
import { CreateAdditionalItemDto } from './additional-item.dto';
import { AdditionalItemService } from './additional-item.service';

@Controller('additional-item')
@ApiTags('additional-item')
export class AdditionalItemController {
  constructor(private readonly _addtionalItemService: AdditionalItemService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: AdditionalItemEntity })
  async createAdditionalItem(
    @Body() request: CreateAdditionalItemDto,
  ): Promise<AdditionalItemEntity> {
    return this._addtionalItemService.createAdditionalItem(request);
  }
}
