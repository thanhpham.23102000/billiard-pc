/* tslint:disable:naming-convention */

import { Transform, TransformFnParams } from 'class-transformer';

export function ToInt(): any {
  return Transform((value: TransformFnParams) => parseInt(value.value, 10), {
    toClassOnly: true,
  });
}
