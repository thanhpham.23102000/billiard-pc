import {
  RESERVATION_REPOSITORY,
  TABLE_CATEGORY_REPOSITORY,
} from 'src/common/constants';
import { ReservationEntity, TableCategoryEntity } from 'src/common/entities';
import { configMySQLDataSource } from 'src/config/const';
import { DataSource } from 'typeorm';

export const reservationProviders = [
  {
    provide: RESERVATION_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(ReservationEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: TABLE_CATEGORY_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TableCategoryEntity),
    inject: [configMySQLDataSource],
  },
];
