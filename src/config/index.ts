import MySQLConfig from './mysql.config';
import LogConfig from './log.config';

export default [MySQLConfig, LogConfig];
