import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { TableCategoryEntity } from './table-category.entity';
import { TableOrderEntity } from './table-order.entity';

export enum TableStatus {
  AVAILABLE = 'available',
  UNAVAILABLE = 'unavailable',
  BOOKED = 'booked',
}

@Entity({ name: 'tables' })
export class TableEntity extends BaseEntity {
  constructor(partial: Partial<TableEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', unique: true, nullable: false })
  name!: string;

  @Column({ type: 'enum', enum: TableStatus, default: TableStatus.AVAILABLE })
  status!: TableStatus;

  @ManyToOne(() => TableCategoryEntity, (tableCategory) => tableCategory.tables)
  tableCategory!: TableCategoryEntity;

  @OneToMany(() => TableOrderEntity, (tableOrder) => tableOrder.table)
  tableOrders: TableOrderEntity[];
}
