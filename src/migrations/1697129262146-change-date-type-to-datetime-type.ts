import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeDateTypeToDatetimeType1697129262146
  implements MigrationInterface
{
  name = 'ChangeDateTypeToDatetimeType1697129262146';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`reservations\` DROP COLUMN \`booking_time\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` ADD \`booking_time\` datetime NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` DROP COLUMN \`booking_time\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` ADD \`booking_time\` datetime NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`orders\` DROP COLUMN \`booking_time\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`orders\` ADD \`booking_time\` date NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` DROP COLUMN \`booking_time\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`reservations\` ADD \`booking_time\` date NOT NULL`,
    );
  }
}
