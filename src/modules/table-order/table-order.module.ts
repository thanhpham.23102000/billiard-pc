import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { tableOrderProviders } from './table-order.provider';
import { TableOrderService } from './table-order.service';
import { TableOrderController } from './table-order.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [TableOrderController],
  providers: [...tableOrderProviders, TableOrderService],
})
export class TableOrderModule {}
