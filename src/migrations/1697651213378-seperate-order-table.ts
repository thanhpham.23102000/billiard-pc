import { MigrationInterface, QueryRunner } from 'typeorm';

export class SeperateOrderTable1697651213378 implements MigrationInterface {
  name = 'SeperateOrderTable1697651213378';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`table_order_details\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`start_time\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`end_time\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`net_price\` decimal(18,6) NOT NULL, \`table_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`addtional_item_order_details\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`quantity\` int NOT NULL, \`additional_item_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_order_details\` ADD CONSTRAINT \`FK_e6de6c5bc65345f3062cc43bd5c\` FOREIGN KEY (\`table_id\`) REFERENCES \`tables\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_order_details\` ADD CONSTRAINT \`FK_58ec114c2f59231a0965fe1a571\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order_details\` ADD CONSTRAINT \`FK_515bdf70eaffaf19da7971566a4\` FOREIGN KEY (\`additional_item_id\`) REFERENCES \`additional_items\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order_details\` ADD CONSTRAINT \`FK_807a9234b8cfe7583dde7251876\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order_details\` DROP FOREIGN KEY \`FK_807a9234b8cfe7583dde7251876\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order_details\` DROP FOREIGN KEY \`FK_515bdf70eaffaf19da7971566a4\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_order_details\` DROP FOREIGN KEY \`FK_58ec114c2f59231a0965fe1a571\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_order_details\` DROP FOREIGN KEY \`FK_e6de6c5bc65345f3062cc43bd5c\``,
    );
    await queryRunner.query(`DROP TABLE \`addtional_item_order_details\``);
    await queryRunner.query(`DROP TABLE \`table_order_details\``);
  }
}
