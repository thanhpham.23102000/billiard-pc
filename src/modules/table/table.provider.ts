import { DataSource } from 'typeorm';
import { configMySQLDataSource } from 'src/config/const';
import { TableCategoryEntity, TableEntity } from 'src/common/entities';
import {
  TABLE_CATEGORY_REPOSITORY,
  TABLE_REPOSITORY,
} from 'src/common/constants/repository.constants';

export const tableProviders = [
  {
    provide: TABLE_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TableEntity),
    inject: [configMySQLDataSource],
  },
  {
    provide: TABLE_CATEGORY_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TableCategoryEntity),
    inject: [configMySQLDataSource],
  },
];
