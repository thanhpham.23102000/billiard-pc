import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAdditionalItemDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  price: number;
}
