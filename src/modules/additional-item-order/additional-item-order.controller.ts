import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { OrderEntity, AdditionalItemOrderEntity } from 'src/common/entities';
import { CreateAdditionalItemOrderDto } from './additional-item-order.dto';
import { AdditionalItemOrderService } from './additional-item-order.service';

@Controller('additional-item-order')
@ApiTags('additional-item-order')
export class AdditionalItemOrderController {
  constructor(
    private readonly _additionalItemOrderService: AdditionalItemOrderService,
  ) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: OrderEntity })
  async createAdditionalItemOrder(
    @Body() request: CreateAdditionalItemOrderDto,
  ): Promise<AdditionalItemOrderEntity> {
    return this._additionalItemOrderService.createAdditionalItemOrder(request);
  }
}
