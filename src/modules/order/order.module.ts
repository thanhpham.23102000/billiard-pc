import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { orderProviders } from './order.provider';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
  imports: [DatabaseModule],
  controllers: [OrderController],
  providers: [...orderProviders, OrderService],
})
export class OrderModule {}
