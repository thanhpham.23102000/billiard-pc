import { IsNotEmpty, IsString } from 'class-validator';
export abstract class AbstractUpdateDto {
  @IsNotEmpty()
  @IsString()
  id!: string;
}
