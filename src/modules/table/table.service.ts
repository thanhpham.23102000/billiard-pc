import { Inject, Injectable } from '@nestjs/common';
import { CreateTableDto } from './table.dto';
import { Repository } from 'typeorm';
import { TableCategoryEntity, TableEntity } from 'src/common/entities';
import {
  BadRequestException,
  InternalServerException,
  NotfoundException,
} from 'src/common/exceptions';
import {
  TABLE_CATEGORY_REPOSITORY,
  TABLE_REPOSITORY,
} from 'src/common/constants/repository.constants';

@Injectable()
export class TableService {
  constructor(
    @Inject(TABLE_REPOSITORY)
    private readonly _tableRepository: Repository<TableEntity>,
    @Inject(TABLE_CATEGORY_REPOSITORY)
    private readonly _tableCategoryRepository: Repository<TableCategoryEntity>,
  ) {}

  public async createTable(tableDto: CreateTableDto): Promise<TableEntity> {
    try {
      const tableCategory = await this._tableCategoryRepository.findOneBy({
        id: tableDto.tableCategoryId,
      });

      if (!tableCategory) {
        throw new NotfoundException('Table category is not found');
      }

      const table = new TableEntity(tableDto);
      table.tableCategory = tableCategory;
      const res = await this._tableRepository.save(table);
      return res;
    } catch (error: any) {
      throw new InternalServerException(`Error creating table: ${error}`);
    }
  }

  public async getOneTable(id: string): Promise<TableEntity> {
    const table = await this._tableRepository.findOne({
      where: {
        id,
      },
      relations: ['tableCategory'],
    });

    if (!table) {
      throw new BadRequestException('Table not found');
    }

    return table;
  }
}
