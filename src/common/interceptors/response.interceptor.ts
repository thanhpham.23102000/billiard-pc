import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    if ('/api'.includes(request.route.path)) {
      return next.handle();
    }

    return next.handle().pipe(
      map((data) => {
        context.switchToHttp().getResponse().status(HttpStatus.OK);
        if (data?.message) {
          return {
            isOk: true,
            message: data.message,
            data: data.data ?? [],
          };
        }
        return { isOk: true, message: '', data };
      }),
    );
  }
}
