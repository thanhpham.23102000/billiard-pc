## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Setup local

1. MySQL

```bash
$ docker run --name billiard_mysql_db -v mysql:/var/lib/mysql -e MYSQL_USER=billiard -e MYSQL_PASSWORD=billiard -e MYSQL_DATABASE=billiard -e MYSQL_ROOT_PASSWORD=billiard -p 3306:3306 -d mysql:8
```

```migration
gen migration: file_name=sample-migration yarn typeorm:generate
create new empty migration: file_name=sample-migration yarn typeorm:create
run migration: yarn typeorm:up
rollback: yarn typeorm:down
```

```diagram
https://app.diagrams.net/#G1NMN5U6-FYr-PGfY2PmF-ZjUhuRv-7Yaz
```
