import { Module } from '@nestjs/common';
import { TableController } from './table.controller';
import { TableService } from './table.service';
import { DatabaseModule } from '../db/db.module';
import { tableProviders } from './table.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [TableController],
  providers: [...tableProviders, TableService],
})
export class TableModule {}
