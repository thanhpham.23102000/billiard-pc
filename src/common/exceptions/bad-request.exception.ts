import { HttpException, HttpStatus } from '@nestjs/common';

export class BadRequestException extends HttpException {
  constructor(message: string, data?: unknown) {
    super(message, HttpStatus.BAD_REQUEST, data);
  }
}
