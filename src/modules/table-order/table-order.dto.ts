import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateTableOrderDto {
  @IsNotEmpty()
  @IsString()
  tableId: string;

  @IsNotEmpty()
  @IsString()
  orderId: string;

  // set default price = 0 when creating new table-order record
  @IsNotEmpty()
  @IsNumber()
  netPrice: number = 0;
}
