import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import {
  ORDER_REPOSITORY,
  TABLE_ORDER_REPOSITORY,
  TABLE_REPOSITORY,
} from 'src/common/constants';
import {
  OrderEntity,
  TableEntity,
  TableOrderEntity,
} from 'src/common/entities';
import { Repository } from 'typeorm';
import { CreateTableOrderDto } from './table-order.dto';
import { NotfoundException } from 'src/common/exceptions';
import { plainToClass } from 'class-transformer';

@Injectable()
export class TableOrderService {
  constructor(
    @Inject(TABLE_ORDER_REPOSITORY)
    private readonly _tableOrderRepository: Repository<TableOrderEntity>,
    @Inject(TABLE_REPOSITORY)
    private readonly _tableRepository: Repository<TableEntity>,
    @Inject(ORDER_REPOSITORY)
    private readonly _orderRepository: Repository<OrderEntity>,
  ) {}

  public async createTableOrder(
    createTableOrderDto: CreateTableOrderDto,
  ): Promise<TableOrderEntity> {
    try {
      const table = await this._tableRepository.findOneBy({
        id: createTableOrderDto.tableId,
      });

      if (!table) {
        throw new NotfoundException('Table is not found');
      }

      const order = await this._orderRepository.findOneBy({
        id: createTableOrderDto.orderId,
      });

      if (!order) {
        throw new NotfoundException('Order is not found');
      }

      const tableOrder = new TableOrderEntity(
        createTableOrderDto,
      );
      tableOrder.table = table;
      tableOrder.order = order;
      const res = await this._tableOrderRepository.save(tableOrder);
      return plainToClass(TableOrderEntity, res);
    } catch (error: any) {
      throw new HttpException(
        `Error creating table category: ${error}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
