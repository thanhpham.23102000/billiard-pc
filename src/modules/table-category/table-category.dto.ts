import { IsNotEmpty, IsString, IsNumber, IsOptional } from 'class-validator';
import { AbstractUpdateDto } from 'src/common/dtos';

export class CreateTableCategoryDto {
  @IsNotEmpty()
  @IsString()
  name!: string;

  @IsNotEmpty()
  @IsNumber()
  unitPrice!: number;

  @IsString()
  @IsOptional()
  description: string;
}

export class UpdateTableCategoryDto extends AbstractUpdateDto {
  @IsOptional()
  @IsString()
  name!: string;

  @IsOptional()
  @IsNumber()
  unitPrice!: number;

  @IsString()
  @IsOptional()
  description: string;
}
