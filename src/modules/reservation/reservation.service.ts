import { Inject, Injectable } from '@nestjs/common';
import {
  RESERVATION_REPOSITORY,
  TABLE_CATEGORY_REPOSITORY,
} from 'src/common/constants';
import { ReservationEntity, TableCategoryEntity } from 'src/common/entities';
import { Repository } from 'typeorm';
import { CreateReservationDto } from './reservation.dto';
import {
  InternalServerException,
  NotfoundException,
} from 'src/common/exceptions';

@Injectable()
export class ReservationService {
  constructor(
    @Inject(RESERVATION_REPOSITORY)
    private readonly _reservationRepository: Repository<ReservationEntity>,
    @Inject(TABLE_CATEGORY_REPOSITORY)
    private readonly _tableCategoryRepository: Repository<TableCategoryEntity>,
  ) {}

  public async createReservation(
    reservationDto: CreateReservationDto,
  ): Promise<ReservationEntity> {
    try {
      const tableCategory = await this._tableCategoryRepository.findOneBy({
        id: reservationDto.tableCategoryId,
      });

      if (!tableCategory) {
        throw new NotfoundException('Table category is not found');
      }

      const reservation = new ReservationEntity(reservationDto);
      reservation.tableCategory = tableCategory;
      const res = await this._reservationRepository.save(reservation);
      return res;
    } catch (error: any) {
      throw new InternalServerException(`Error creating reservation: ${error}`);
    }
  }
}
