import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameTable1697970527982 implements MigrationInterface {
  name = 'RenameTable1697970527982';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`table_orders\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`start_time\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`end_time\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`net_price\` decimal(18,6) NOT NULL, \`table_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`addtional_item_order\` (\`id\` varchar(36) NOT NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`quantity\` int NOT NULL, \`additional_item_id\` varchar(36) NULL, \`order_id\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_orders\` ADD CONSTRAINT \`FK_b91bda746ffd65afc03e8fa973a\` FOREIGN KEY (\`table_id\`) REFERENCES \`tables\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_orders\` ADD CONSTRAINT \`FK_fad7f12f9852ecc30efb2b2d925\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order\` ADD CONSTRAINT \`FK_6c5e62358d0f5b2e5c2df9a3e9e\` FOREIGN KEY (\`additional_item_id\`) REFERENCES \`additional_items\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order\` ADD CONSTRAINT \`FK_a4edc86dad5867280b9c7dc8cbb\` FOREIGN KEY (\`order_id\`) REFERENCES \`orders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order\` DROP FOREIGN KEY \`FK_a4edc86dad5867280b9c7dc8cbb\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`addtional_item_order\` DROP FOREIGN KEY \`FK_6c5e62358d0f5b2e5c2df9a3e9e\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_orders\` DROP FOREIGN KEY \`FK_fad7f12f9852ecc30efb2b2d925\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`table_orders\` DROP FOREIGN KEY \`FK_b91bda746ffd65afc03e8fa973a\``,
    );
    await queryRunner.query(`DROP TABLE \`addtional_item_order\``);
    await queryRunner.query(`DROP TABLE \`table_orders\``);
  }
}
