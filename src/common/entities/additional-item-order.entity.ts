import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './abstract.entity';
import { AdditionalItemEntity } from './additional-item.entity';
import { OrderEntity } from './order.entity';

@Entity({ name: 'addtional_item_orders' })
export class AdditionalItemOrderEntity extends BaseEntity {
  constructor(partial: Partial<AdditionalItemOrderEntity>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'int' })
  quantity: number;

  @ManyToOne(
    () => AdditionalItemEntity,
    (additionalItem) => additionalItem.additionalItemOrders,
  )
  additionalItem!: AdditionalItemEntity;

  @ManyToOne(() => OrderEntity, (order) => order.additionalItemOrder)
  order!: OrderEntity;
}
