import { Inject, Injectable } from '@nestjs/common';
import { ADDITIONAL_ITEM_REPOSITORY } from 'src/common/constants';
import { AdditionalItemEntity } from 'src/common/entities';
import { Repository } from 'typeorm';
import { CreateAdditionalItemDto } from './additional-item.dto';
import { InternalServerException } from 'src/common/exceptions';

@Injectable()
export class AdditionalItemService {
  constructor(
    @Inject(ADDITIONAL_ITEM_REPOSITORY)
    private readonly _additionalItemRepository: Repository<AdditionalItemEntity>,
  ) {}

  public async createAdditionalItem(
    additionalItemDto: CreateAdditionalItemDto,
  ): Promise<AdditionalItemEntity> {
    try {
      const additionalItem = new AdditionalItemEntity(additionalItemDto);
      const res = await this._additionalItemRepository.save(additionalItem);
      return res;
    } catch (error: any) {
      throw new InternalServerException(
        `Error creating additional item: ${error}`,
      );
    }
  }
}
