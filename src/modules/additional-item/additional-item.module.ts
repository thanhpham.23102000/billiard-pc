import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { additionalItemProviders } from './additional-item.provider';
import { AdditionalItemController } from './additional-item.controller';
import { AdditionalItemService } from './additional-item.service';

@Module({
  imports: [DatabaseModule],
  controllers: [AdditionalItemController],
  providers: [...additionalItemProviders, AdditionalItemService],
})
export class AdditionalItemModule {}
