import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { OrderEntity, TableOrderEntity } from 'src/common/entities';
import { TableOrderService } from './table-order.service';
import { CreateTableOrderDto } from './table-order.dto';

@Controller('table-order')
@ApiTags('table-order')
export class TableOrderController {
  constructor(
    private readonly _tableOrderService: TableOrderService,
  ) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: OrderEntity })
  async createTableOrder(
    @Body() request: CreateTableOrderDto,
  ): Promise<TableOrderEntity> {
    return this._tableOrderService.createTableOrder(request);
  }
}
