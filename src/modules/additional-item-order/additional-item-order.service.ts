import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import {
  ADDITIONAL_ITEM_ORDER_REPOSITORY,
  ADDITIONAL_ITEM_REPOSITORY,
  ORDER_REPOSITORY,
} from 'src/common/constants';
import {
  AdditionalItemEntity,
  AdditionalItemOrderEntity,
  OrderEntity,
} from 'src/common/entities';
import { Repository } from 'typeorm';
import { NotfoundException } from 'src/common/exceptions';
import { CreateAdditionalItemOrderDto } from './additional-item-order.dto';

@Injectable()
export class AdditionalItemOrderService {
  constructor(
    @Inject(ADDITIONAL_ITEM_REPOSITORY)
    private readonly _additionalItemRepository: Repository<AdditionalItemEntity>,
    @Inject(ADDITIONAL_ITEM_ORDER_REPOSITORY)
    private readonly _additionalItemOrderRepository: Repository<AdditionalItemOrderEntity>,
    @Inject(ORDER_REPOSITORY)
    private readonly _orderRepository: Repository<OrderEntity>,
  ) {}

  public async createAdditionalItemOrder(
    createAdditionalItemOrderDto: CreateAdditionalItemOrderDto,
  ): Promise<AdditionalItemOrderEntity> {
    try {
      const additionalItem = await this._additionalItemRepository.findOneBy({
        id: createAdditionalItemOrderDto.additionalItemId,
      });

      if (!additionalItem) {
        throw new NotfoundException('Additional item is not found');
      }

      const order = await this._orderRepository.findOneBy({
        id: createAdditionalItemOrderDto.orderId,
      });

      if (!order) {
        throw new NotfoundException('Order is not found');
      }

      const additionalItemOrder = new AdditionalItemOrderEntity(
        createAdditionalItemOrderDto,
      );
      additionalItemOrder.additionalItem = additionalItem;
      additionalItemOrder.order = order;
      const res =
        await this._additionalItemOrderRepository.save(additionalItemOrder);
      return res;
    } catch (error: any) {
      throw new HttpException(
        `Error creating additional item order: ${error}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
