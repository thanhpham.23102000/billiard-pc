import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/db.module';
import { TableCategoryController } from './table-category.controller';
import { tableCategoryProviders } from './table-category.provider';
import { TableCategoryService } from './table-category.service';

@Module({
  imports: [DatabaseModule],
  controllers: [TableCategoryController],
  providers: [...tableCategoryProviders, TableCategoryService],
})
export class TableCategoryModule {}
